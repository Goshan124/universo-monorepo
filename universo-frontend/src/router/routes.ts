import { RouteRecordRaw } from 'vue-router';
import { useCurrentUserStore } from 'src/stores/current-user';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'start',
    meta: { title: 'Start' },
    component: () => import('layouts/IndexLayout.vue'),
    beforeEnter: (to, from) => {
      const store = useCurrentUserStore();
      if (store.getUserId) {
        return { name: 'projects' };
      }
      return;
    },
  },
  {
    path: '/projects',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/projects/:uuid/',
        name: 'project',
        meta: { title: 'Project' },
        component: () => import('pages/CurrentProjectPage.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          if (!store.getUserId) {
            return { name: 'start' };
          }
          return;
        },
      },
      {
        path: '',
        name: 'projects',
        meta: { title: 'Projects' },
        component: () => import('pages/ProjectsPage.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          if (!store.getUserId) {
            return { name: 'start' };
          }
          return;
        },
      },
      {
        path: '/login',
        name: 'login',
        meta: { title: 'Login' },
        component: () => import('pages/LoginPage.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          if (store.getUserId) {
            return { name: 'projects' };
          }
          return;
        },
      },
      {
        path: '/registration',
        name: 'registration',
        meta: { title: 'registration' },
        component: () => import('pages/RegistrationPage.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          if (store.getUserId) {
            return { name: 'projects' };
          }
          return;
        },
      },
      {
        path: '/profile',
        name: 'profile',
        meta: { title: 'Profile' },
        component: () => import('pages/UserProfile.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          if (!store.getUserId) {
            return { name: 'start' };
          }
          return;
        },
      },

      {
        path: '/privacypolicy',
        name: 'privacypolicy',
        component: () => import('pages/PrivacyPolicyPage.vue'),
      },
    ],
  },
  {
    path: '/spaces',
    component: () => import('layouts/CanvasLayout.vue'),
    children: [
      {
        path: '/spaces/:uuid/',
        name: 'Spaces',
        meta: { title: 'Spaces' },
        component: () => import('../apps/spaces-2d/Spaces2D.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          if (!store.getUserId) {
            return { name: 'start' };
          }
          return;
        },
      },
    ],
  },
  {
    path: '/dev',
    component: () => import('layouts/CanvasLayout.vue'),
    children: [
      {
        path: 'tsi/:uuid/',
        name: 'testspace-i',
        meta: { title: 'Test Space Integration' },
        component: () =>
          import('../apps/develop/integration/TestSpaceIntegration.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          // if (!store.getUserId) {
          //   return { name: 'start' };
          // }
          return;
        },
      },
      {
        path: 'tso/:uuid/',
        name: 'testspace-o',
        meta: { title: 'Test Space Objects' },
        component: () => import('../apps/develop/objects/TestSpaceObjects.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          // if (!store.getUserId) {
          //   return { name: 'start' };
          // }
          return;
        },
      },
      {
        path: 'tsb/:uuid/',
        name: 'testspace-b',
        meta: { title: 'Test Space Basic' },
        component: () => import('../apps/develop/basic/TestSpaceBasic.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          // if (!store.getUserId) {
          //   return { name: 'start' };
          // }
          return;
        },
      },
      {
        path: 'tse/:uuid/',
        name: 'testspace-e',
        meta: { title: 'Test Space Effects' },
        component: () => import('../apps/develop/effects/TestSpaceEffects.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          // if (!store.getUserId) {
          //   return { name: 'start' };
          // }
          return;
        },
      },
      {
        path: 'tsv/:uuid/',
        name: 'testspace-v',
        meta: { title: 'Test Space Visualization' },
        component: () =>
          import('../apps/develop/visualization/TestSpaceVisualization.vue'),
        beforeEnter: (to, from) => {
          const store = useCurrentUserStore();
          // if (!store.getUserId) {
          //   return { name: 'start' };
          // }
          return;
        },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
