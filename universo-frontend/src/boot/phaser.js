import { boot } from 'quasar/wrappers';

import * as Phaser from 'phaser/dist/phaser.esm';
// import RexUIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin.js';
//TODO: Нужно изучить вопрос подключения плагина. Сейчас, если его просто импортировать, то он не видит Phaser,
// соответственно приложение падает с ошибкой. То же самое происходит и при подключении непосредственно в экземпляр
// Phaser через preload из удалённого репозитория в виде скрипта

// let Phaser = null;
// let rexUI = null;

export default boot(async ({ app }) => {
  // Phaser = await import('phaser')
  //   .then((module) => {
  //     console.log(module);

  //     return module;
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //   });

  // console.log(Phaser);

  // rexUI = await import('phaser3-rex-plugins/templates/ui/ui-plugin.js');

  // console.log(rexUI);

  app.config.globalProperties.$phaser = Phaser;
  // app.config.globalProperties.$rexUI = rexUI;
});

export {
  Phaser,

  // rexUI
};
