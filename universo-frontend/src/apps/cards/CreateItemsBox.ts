// import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateItem from './CreateItem';
import AddDragDropItemBehavior from '../kanbans/AddDragDropItemBehavior';
import { ObjectWrapper } from 'src/types/stores/kanvaso';
import { ItemsBoxType, EndlessCanvas } from 'src/types/kanban_types';

const CreateItemsBox = (
  scene: EndlessCanvas,
  cards: ObjectWrapper[],
  registry: Phaser.Data.DataManager,
) => {
  const itemsBox = scene.rexUI.add.sizer({
    orientation: 'y',
    space: {
      left: 5,
      right: 5,
      top: 5,
      bottom: 5,
      item: 5,
    },
  }) as undefined as ItemsBoxType;
  //@ts-ignore

  itemsBox.addBackground(scene.rexUI.add.roundRectangle({}), 'background');
  if (cards.length) {
    for (let i = 0; i < cards.length; i++) {
      itemsBox.add(CreateItem(scene, cards[i], registry), {
        proportion: 0,
        expand: true,
      });
    }
  }

  AddDragDropItemBehavior(itemsBox);

  return itemsBox;
};

export default CreateItemsBox;
