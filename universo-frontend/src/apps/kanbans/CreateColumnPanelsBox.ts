import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateColumnPanel from './CreateColumnPanel';
import AddDragDropColumnPanelBehavior from './AddDragDropColumnPanelBehavior';
import { ObjectWrapper } from 'src/types/stores/kanvaso';
import { PanelsBoxType, EndlessCanvas } from 'src/types/kanban_types';

const CreateColumnPanelsBox = (
  scene: EndlessCanvas,
  swimlane: ObjectWrapper,
  registry: Phaser.Data.DataManager,
) => {
  const config = {
    orientation: 'x',
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
    },
  };
  //@ts-ignore

  const columnPanelsBox = scene.rexUI.add.sizer(config) as PanelsBoxType;
  // .addBackground(
  //     scene.rexUI.add.roundRectangle({
  //         strokeColor: COLOR_PRIMARY,
  //         strokeWidth: 3,
  //     }),
  //     'background'
  // )
  if (swimlane && swimlane.childrens && swimlane.childrens.length > 0) {
    swimlane.childrens.forEach((column: ObjectWrapper) => {
      const columnPanel = CreateColumnPanel(scene, column, registry);
      columnPanelsBox.add(columnPanel, { proportion: 0, expand: true });
    });
  }

  AddDragDropColumnPanelBehavior(columnPanelsBox); // Убедитесь, что функция AddDragDropColumnPanelBehavior определена где-то в вашем коде

  return columnPanelsBox;
};

export default CreateColumnPanelsBox;
