// import { COLOR_PRIMARY } from './Const'; // Удалил неиспользуемые переменные
// import GetNeighborObjects from './GetNeighborObjects';

import { PanelsBoxType } from 'src/types/kanban_types';

const AddDragDropColumnPanelBehavior = (panelsBox: PanelsBoxType) => {
  const panels = panelsBox.getElement('items');
  for (const panel of panels) {
    panel
      .on('sizer.dragstart', function () {
        console.log(panel.children[1].children[0].children[1].text);
        panelsBox.remove(panel);
        panel.layout();
      })
      .on('sizer.dragend', function (pointer: Phaser.Input.Pointer) {
        // Удалил неиспользуемые переменные
        panelsBox.insertAtPosition(pointer.x, pointer.y, panel, {
          expand: true,
        });
        ArrangePanels(panelsBox);
      });
  }
};

const ArrangePanels = (panelsBox: PanelsBoxType) => {
  const panels = panelsBox.getElement('items');

  for (const panel of panels) {
    panel.setData({ startX: panel.x, startY: panel.y });
  }

  panelsBox.getTopmostSizer().layout();

  for (const panel of panels) {
    const fromX = panel.getData('startX') as number;
    const fromY = panel.getData('startY') as number;
    if (panel.x !== fromX || panel.y !== fromY) {
      panel.moveFrom({ x: fromX, y: fromY, speed: 300 });
    }
  }
};

export default AddDragDropColumnPanelBehavior;
