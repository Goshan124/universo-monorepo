import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import GetMaxTextObjectSize from './GetMaxTextObjectSize';
import CreateTextObject from './CreateTextObject';
import CreatePopupList from './CreatePopupList';
import { EndlessCanvas } from 'src/types/kanban_types';
import { ObjectWrapper } from 'src/types/stores/kanvaso';

const CreateDropDownList = function (
  scene: EndlessCanvas,
  board: ObjectWrapper,
) {
  const options = ['Добавить столбец', 'Переименовать доску'];
  const maxTextSize = GetMaxTextObjectSize(scene, options);
  const label = scene.rexUI.add
    .label({
      background: scene.rexUI.add
        .roundRectangle(0, 0, 2, 2, 0, COLOR_PRIMARY)
        .setAlpha(0),
      text: CreateTextObject(scene, '...').setFixedSize(
        maxTextSize.width,
        maxTextSize.height,
      ),
      space: {
        left: 10,
        right: 10,
        top: 10,
        bottom: 10,
        icon: 10,
      },
    })
    .setData('value', '');

  let menu;

  scene.rexUI.add.click(label).on('click', () => {
    if (!menu || !menu.scene) {
      //@ts-ignore

      const menuX = label.getElement('text').getTopLeft().x;
      const menuY = label.bottom;
      menu = CreatePopupList(scene, board, menuX, menuY, options, function (button) {
        console.log('Click', button.text); // Добавленный console.log
        if (button.text === 'Добавить столбец') {
          // Ваш код для добавления новой колонки
        } else if (button.text === 'Переименовать доску') {
          // Ваш код для переименования доски
          console.log('button', button);
        }
        menu?.collapse();
        menu = undefined;
      });
    } else {
      menu.collapse();
      menu = undefined;
    }
  });

  return label;
};
export default CreateDropDownList;
