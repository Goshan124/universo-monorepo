// import stretchingIcon from './Stretching_icon1.png';

const AddResizeController = (sizer): void => {
  const scene = sizer.scene;

  const offsetX = 20;
  const offsetY = 20;

  const bottomRighterController = scene.add.image(
    sizer.right - offsetX,
    sizer.bottom - offsetY,
    'icon',
  );

  let dragOffsetX = 0;
  let dragOffsetY = 0;

  bottomRighterController.setInteractive({ draggable: true });

  bottomRighterController.on(
    'dragstart',
    function (pointer: Phaser.Input.Pointer) {
      dragOffsetX = pointer.x - bottomRighterController.x;
      dragOffsetY = pointer.y - bottomRighterController.y;
    },
  );

  bottomRighterController.on(
    'drag',
    function (pointer: Phaser.Input.Pointer, dragX, dragY) {
      const topX = sizer.left,
        topY = sizer.top;
      const width = dragX + dragOffsetX - topX,
        height = dragY + dragOffsetY - topY;

      sizer.setMinSize(width, height).layout();
    },
  );

  // Обновляем позицию AddResizeController каждый раз, когда происходит обновление сцены
  scene.events.on('update', function () {
    bottomRighterController.setPosition(
      sizer.right - offsetX,
      sizer.bottom - offsetY,
    );
  });

  sizer.pin(bottomRighterController);
};

export default AddResizeController;
