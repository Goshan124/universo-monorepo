import { ObjectWrapper, TreeNode } from 'src/types/stores/kanvaso';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateDropDownList from './CreateDropDownList';
import { EndlessCanvas } from 'src/types/kanban_types';

const CreateHeader = function (
  scene: EndlessCanvas,
  board: ObjectWrapper,
  registry: Phaser.Data.DataManager,
) {
  const sizer = scene.rexUI.add
    .sizer({
      orientation: 'x',
    })
    .addBackground(scene.rexUI.add.roundRectangle(0, 0, 20, 20, 0, COLOR_DARK));
  const headerLabel = scene.rexUI.add.label({
    text: scene.add.text(0, 0, board.name),
  });
  registry.events.on('changedata', (parent, key, data, previousData) => {
    if (data.uuid === board.uuid) {
      headerLabel.setText(data.name);
    }
  });
  const dropDownButton = CreateDropDownList(scene, board);

  sizer
    .add(headerLabel, { proportion: 1, expand: true })
    .add(dropDownButton, { proportion: 0, expand: true });

  return sizer;
};
export default CreateHeader;
