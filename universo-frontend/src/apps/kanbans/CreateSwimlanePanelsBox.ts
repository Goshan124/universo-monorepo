import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateColumnPanel from './CreateColumnPanel';
import AddDragDropColumnPanelBehavior from './AddDragDropColumnPanelBehavior';
import { ObjectWrapper } from 'src/types/stores/kanvaso';
import CreateColumnPanelsBox from './CreateColumnPanelsBox';
import CreateHeader from './CreateHeader';
import { PanelsBoxType, EndlessCanvas } from 'src/types/kanban_types';

const CreateSwimlanePanelBox = (
  scene: EndlessCanvas,
  board: ObjectWrapper,
  registry: Phaser.Data.DataManager,
) => {
  const config = {
    orientation: 'y',
    sliderX: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    sliderY: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    scrollerX: false,
    scrollerY: false,
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
      sliderX: 10,
      sliderY: 10,
    },
  };
  //@ts-ignore

  const swimlanePanelsBox = scene.rexUI.add.sizer(config) as PanelsBoxType;
  //   .addBackground(
  //   scene.rexUI.add.roundRectangle({
  //     strokeColor: COLOR_PRIMARY,
  //     strokeWidth: 3,
  //   }),
  //   'background',
  // );

  if (board && board.childrens && board.childrens.length > 0) {
    board.childrens.forEach((swimlane: ObjectWrapper) => {
      const swimlanePanel = CreateColumnPanelsBox(scene, swimlane, registry);
      swimlanePanelsBox.add(swimlanePanel, {
        proportion: 0,
        expand: true,
      });
    });
  }

  AddDragDropColumnPanelBehavior(swimlanePanelsBox); // Убедитесь, что функция AddDragDropColumnPanelBehavior определена где-то в вашем коде

  return swimlanePanelsBox;
};

export default CreateSwimlanePanelBox;
