import { ObjectWrapper, TreeNode } from 'src/types/stores/kanvaso';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import { DefaultDepth, DragObjectDepth } from './Const';
import CreateItemsBox from '../cards/CreateItemsBox';
import { EndlessCanvas } from 'src/types/kanban_types';
import CreateRequestModal from '../kanbans/CreateRequestModal';

const CreateColumnPanel = (
  scene: EndlessCanvas,
  column: ObjectWrapper,
  registry: Phaser.Data.DataManager,
) => {
  const panel = scene.rexUI.add
    .dialog({
      width: 120,
      space: { left: 10, right: 10, top: 10, bottom: 10 },
      //@ts-ignore

      background: scene.rexUI.add.roundRectangle({
        strokeColor: COLOR_DARK,
        radius: 0,
      }),
      title: CreateTitle(scene, column, registry, () => {
        panel.layout();
      }),
      content: CreateItemsBox(
        scene,
        column.childrens ? column.childrens : [],
        registry,
      ),
      proportion: {
        content: 1,
      },
    })
    .setOrigin(0, 0);

  SetDraggable(panel);

  return panel;
};

const CreateTitle = (
  scene: EndlessCanvas,
  column: ObjectWrapper,
  registry: Phaser.Data.DataManager,
  callback,
) => {
  let textInput = '';
  const itemGroup = scene.add.group();
  const icon = scene.add.image(0, 0, 'editicon').setDepth(1);
  icon.setInteractive({ cursor: 'pointer' });

  icon.on('pointerdown', () => {
    CreateRequestModal(scene, column.name, function (text) {
      console.log(`Content: '${text}'`);
      textInput = text;
    })
      .setPosition(400, 300)
      .layout()
      .modalPromise({
        manualClose: true,
        duration: {
          in: 500,
          out: 500,
        },
      })
      .then(function (data) {
        //@ts-ignore
        if (data.text === 'Сохранить') {
          if (textInput) {
            const payload = {
              nomo: textInput,
              uuid: column.uuid,
              //@ts-ignore
              // kanvasoUuid: scene.store.getKanvaso[0].node.uuid,
            };
            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        }
      });
  });
  const title = scene.rexUI.add.label({
    rtl: true,
    //@ts-ignore

    background: scene.rexUI.add.roundRectangle({
      color: COLOR_LIGHT,
    }),
    text: scene.add.text(0, 0, column.name || '', {
      fontSize: 18,
    }),
    align: 'left',
    action: icon,
    actionSize: 24,
    space: {
      left: 5,
      right: 5,
      top: 5,
      bottom: 5,
      actionRight: 5,
    },
  });

  registry.events.on('changedata', (parent, key, data, previousData) => {
    if (data.uuid === column.uuid) {
      title.setText(data.name);
      callback();
    }
  });

  return title;
};

const SetDraggable = (panel: any) => {
  panel
    .setDraggable({
      sensor: 'title',
      target: panel,
    })
    .on('sizer.dragstart', OnPanelDragStart, panel)
    .on('sizer.dragend', OnPanelDragEnd, panel);
};

const OnPanelDragStart = function (this: any) {
  this.setDepth(DragObjectDepth);
  this.getElement('background').setStrokeStyle(3, 0xff0000);
};

const OnPanelDragEnd = function (this: any) {
  this.setDepth(DefaultDepth);
  this.getElement('background').setStrokeStyle(2, COLOR_DARK);
};

export default CreateColumnPanel;
