import { EndlessCanvas } from 'src/types/kanban_types';

const CreateModalLabel = (scene: EndlessCanvas, text: string) => {
  const normalBackground = scene.rexUI.add.roundRectangle(
    0,
    0,
    0,
    0,
    10,
    0xffffff,
    0.0,
  );
  normalBackground.setStrokeStyle(2, 0x000000); // Чёрная обводка

  const hoverBackground = scene.rexUI.add.roundRectangle(
    0,
    0,
    0,
    0,
    10,
    0xffffff,
    0.0,
  );
  hoverBackground.setStrokeStyle(2, 0x000000); // Чёрная обводка

  const label = scene.rexUI.add
    .label({
      background: normalBackground,
      text: scene.add.text(0, 0, text, {
        fontSize: 18,
        color: '#000000', // Чёрный текст
      }),
      align: 'center',
      space: {
        left: 5,
        right: 5,
        top: 5,
        bottom: 5,
      },
    })
    .on('button.over', function (button, groupName, index, pointer, event) {
      button.getElement('background').destroy();
      button.setElement('background', hoverBackground);
    })
    .on('button.out', function (button, groupName, index, pointer, event) {
      button.getElement('background').destroy();
      button.setElement('background', normalBackground);
    });

  return label;
};
export default CreateModalLabel;
