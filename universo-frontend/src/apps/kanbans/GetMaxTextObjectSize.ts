import { EndlessCanvas } from 'src/types/kanban_types';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateTextObject from './CreateTextObject';

const GetMaxTextObjectSize = function (
  scene: EndlessCanvas,
  contentArray: string[],
): { width: number; height: number } {
  const textObject = CreateTextObject(
    scene,
    '',
  ) as unknown as Phaser.GameObjects.Text;
  let width = 0,
    height = 0;
  for (const content of contentArray) {
    textObject.text = content;
    width = Math.max(textObject.width, width);
    height = Math.max(textObject.height, height);
  }
  textObject.destroy();
  return { width, height };
};

export default GetMaxTextObjectSize;
