// import 'phaser';
// import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import { DefaultDepth, DragObjectDepth } from './Const';
import CreateColumnPanelsBox from './CreateColumnPanelsBox';
import CreateHeader from './CreateHeader';
import AddResizeController from './AddResizeController';
import { ObjectWrapper, TreeNode } from 'src/types/stores/kanvaso';
import CreateSwimlanePanelsBox from './CreateSwimlanePanelsBox';
import { EndlessCanvas } from 'src/types/kanban_types';

const CreateScrollablePanel = (
  scene: EndlessCanvas,
  board: ObjectWrapper,
  registry: Phaser.Data.DataManager,
  size: { w: number; h: number },
) => {
  const config = {
    adaptThumbSizeMode: true,
    width: size.w - 30,
    height: size.h - 30,
    //@ts-ignore
    background: scene.rexUI.add.roundRectangle({
      radius: 10,
      strokeColor: COLOR_DARK,
    }),
    panel: {
      child: CreateColumnPanelsBox(scene, board?.childrens[0], registry),
      mask: {
        padding: 2,
        updateMode: 'everyTick',
      },
    },
    sliderX: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    sliderY: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    scrollerX: false,
    scrollerY: false,
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
      sliderX: 10,
      sliderY: 10,
    },
    header: CreateHeader(scene, board, registry),
  };

  // Создаем ScrollablePanel
  //@ts-ignore

  const scrollablePanel = scene.rexUI.add.scrollablePanel(config);

  // Устанавливаем возможность перетаскивания
  scrollablePanel.setDraggable('header');

  // Вызываем метод layout для обновления компоновки
  scrollablePanel.layout();

  // Добавляем контроллер для изменения размера
  AddResizeController(scrollablePanel);

  return scrollablePanel;
};

const CreateMoreScrollablePanels = (
  scene: EndlessCanvas,
  boards: ObjectWrapper[],
  registry: Phaser.Data.DataManager,
  size: { h: number; w: number },
) =>
  boards.map((board: ObjectWrapper) =>
    CreateScrollablePanel(scene, board, registry, size),
  );
//@ts-ignore
export default CreateMoreScrollablePanels;
