import { EndlessCanvas } from 'src/types/kanban_types';

const CreateTextObject = function (scene: EndlessCanvas, text: string): any {
  return scene.add.text(0, 0, text, {
    fontSize: '20px',
  });
};
export default CreateTextObject;
