import { AUTO, Scale, Types } from 'phaser';

export const getPhaserConfig = (scene: any): Types.Core.GameConfig => ({
  type: AUTO,
  parent: 'phaser-container',
  width: window.innerWidth,
  height: window.innerHeight,
  scale: {
    mode: Scale.FIT,
    autoCenter: Scale.CENTER_BOTH,
  },
  scene: scene,
  plugins: {
    // scene: [
    // {
    // key: 'rexUI',
    // plugin: RexPlugins,
    // mapping: 'rexUI',
    // },
    // // ...
    // ],
  },
});
