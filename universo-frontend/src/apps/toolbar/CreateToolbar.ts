import { EndlessCanvas } from 'src/types/kanban_types';
import { Router } from 'vue-router';

function CreateToolbar(scene: EndlessCanvas, router: Router) {
  let toolbarWidth = scene.toolbarWidth; // Ширина панели инструментов
  let screenHeight = parseInt(scene.sys.game.config.height as string, 10); // Высота экрана

  let toolbar = scene.add.rectangle(0, 0, toolbarWidth, screenHeight, 0x607fd1);
  toolbar.setOrigin(0, 0); // Установить начало координат в верхний левый угол
  toolbar.setDepth(20); // Установка глубины для toolbar

  const back = scene.add.image(0, 0, 'logo').setDepth(1010);
  back.setScale(toolbarWidth / back.width);
  back.setPosition(back.displayWidth / 2, back.displayHeight / 2);
  back.setInteractive({ cursor: 'pointer' });

  back.setInteractive();
  back.on('pointerdown', () => {
    router.push('/projects');
  });
  const toolbar1 = scene.add.group();
  toolbar1.add(toolbar);
  toolbar1.add(back);
  return toolbar1;
}
export default CreateToolbar;
