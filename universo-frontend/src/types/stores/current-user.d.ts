export interface UserResponse {
  objId: number;
  unuaNomo: {
    enhavo: string;
  };
  universoUzanto: { retnomo: string };
  duaNomo: { enhavo: string };
  familinomo: { enhavo: string };
  chefaRetposhto: string;
  avataro: null | {
    bildoE: { url: string } | null;
    bildoF: { url: string } | null;
    id: string | null;
  };
}

export interface State {
  user: Mi | null;
  jwt: string | null;
  isLoggedIn: boolean | null;
  konfirmita: boolean | null;
  isAdmin: boolean | null;
  authtoken: string | null;
  csrftoken: string | null;
  organizations: null | { edges: [{ node: { uuid: number } }] };
  authData: object | null;
  counter: number;
  getJWTpayload?: GetJwtPayload;
  ws_connected: Boolean;
}
export interface Getters {
  getUserId: number;
  getJWTpayload: GetJwtPayload;
}

export interface GetJwtPayload {
  getJWTpayload:
    | string[]
    | { [key: string]: string }[]
    | (string | { [key: string]: string })[];
}
export interface UnuaNomo {
  enhavo: string;
}

export interface DuaNomo {
  enhavo: string;
}

export interface Familinomo {
  enhavo: string;
}

export interface ChefaLingvo {
  id: string;
  nomo: string;
}

export interface Nomo {
  lingvo: string;
  enhavo: string;
  chefaVarianto: boolean;
}

export interface Loghlando {
  nomo: Nomo;
  id: string;
}

export interface Nomo {
  enhavo: string;
}

export interface Loghregiono {
  id: string;
  nomo: Nomo;
}

export interface BildoE {
  url: string;
}

export interface BildoF {
  url: string;
}

export interface Avataro {
  id: string;
  bildoE: BildoE;
  bildoF: BildoF;
}

export interface Statistiko {
  miaGekamarado: boolean;
  miaGekamaradoPeto: boolean;
  kandidatoGekamarado: boolean;
  tutaGekamaradoj: number;
  rating?: any;
  aktivaDato?: any;
}

export interface Nomo {
  enhavo: string;
}

export interface ChefaOrganizo {
  id: string;
  nomo: Nomo;
}

export interface Nomo {
  enhavo: string;
}

export interface Node {
  id: string;
  nomo: Nomo;
}

export interface Edge {
  node: Node;
}

export interface Sovetoj {
  edges: Edge[];
}

export interface Nomo {
  enhavo: string;
}

export interface Node {
  id: string;
  nomo: Nomo;
}

export interface Edge {
  node: Node;
}

export interface Sindikatoj {
  edges: Edge[];
}

export interface AdministritajKomunumoj {
  edges: any[];
}

export interface UniversoUzanto {
  id: string;
  uuid: string;
  retnomo: string;
}

export interface Mi {
  id: string;
  objId: number;
  uuid: string;
  unuaNomo: UnuaNomo;
  duaNomo: DuaNomo;
  familinomo: Familinomo;
  sekso: string;
  konfirmita: boolean;
  isActive: boolean;
  chefaLingvo: ChefaLingvo;
  chefaTelefonanumero: string;
  chefaRetposhto: string;
  naskighdato: string;
  loghlando: Loghlando;
  loghregiono: Loghregiono;
  agordoj: string;
  avataro: Avataro;
  statuso?: any;
  statistiko: Statistiko;
  kontaktaInformo?: any;
  kandidatojTuta: number;
  gekamaradojTuta: number;
  chefaOrganizo: ChefaOrganizo;
  sovetoj: Sovetoj;
  sindikatoj: Sindikatoj;
  administritajKomunumoj: AdministritajKomunumoj;
  universoUzanto: UniversoUzanto;
  isAdmin: boolean;
}

export interface RootObject {
  mi: Mi;
}
