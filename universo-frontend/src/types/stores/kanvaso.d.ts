export type TreeNode = {
  setChildrens(node: ObjectWrapper);
  name: string;
  isPublished: boolean;
  isDeleted: boolean;
  description: string;
  type: string;
  id: number;
  typeId: number;
  uuid: string;
  parentUuid: string;
  childrens: ObjectWrapper[];
  hasParent: boolean;
  _wrappingObject: TreeNode;
  _wrappingObject: {
    childrens?: ObjectWrapper[];
  };

  uuid: string;
  forigo: boolean;
  publikigo: boolean;
  nomo: {
    enhavo: string;
  };
  priskribo: {
    enhavo: string;
  };
  objId: number;
  tipo: Tipo;
  posedanto: {
    edges: [
      {
        node?: {
          uuid: string;
        };
      },
    ];
  };
  kanvasojKanvasoobjektoligiloLigilo: {
    posedanto: {
      uuid: string;
      objId: number;
      nomo: {
        enhavo: string;
      };
      priskribo: {
        enhavo: string;
      };
    };
    tipo: Tipo;
  };

  childrens?: ObjectWrapper[];
};
export type ObjectWrapper = {
  setChildrens(node: ObjectWrapper): void;
  name: string;
  isPublished: boolean;
  isDeleted: boolean;
  description: string;
  type: string;
  id: number;
  typeId: number;
  uuid: string;
  parentUuid: string;
  childrens: ObjectWrapper[];
  hasParent: boolean;
  _wrappingObject: TreeNode;
  _wrappingObject: {
    childrens?: ObjectWrapper[];
  };
};
export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface Node {
  nomo: Nomo;
  priskribo: Priskribo;
  uuid: string;
  objId: number;
  forigo: boolean;
  publikigo: boolean;
}

export interface Edge {
  node: Node;
}

export interface RootObject {
  edges: Edge[];
}

export interface KanvasojKanvasoObjekto {
  edges: any[];
}

export interface RootObject {
  kanvasojKanvasoObjekto: KanvasojKanvasoObjekto;
}
export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface Node {
  nomo: Nomo;
  priskribo: Priskribo;
  uuid: string;
  objId: number;
  forigo: boolean;
  publikigo: boolean;
}

export interface Edge {
  node: Node;
}

export interface KanvasojKanvaso {
  edges: Edge[];
}

export interface RootObject {
  kanvasojKanvaso: KanvasojKanvaso;
}

export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface Kategorio {
  edges: any[];
}

export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface Tipo {
  uuid: string;
  objId: number;
  nomo: Nomo;
  priskribo: Priskribo;
}

export interface Node {
  pozicio?: any;
  objId: number;
  uuid: string;
  forigo: boolean;
  publikigo: boolean;
  nomo: Nomo;
  priskribo: Priskribo;
  tipo: Tipo;
  kanvasojKanvasoobjektoligiloLigilo?: any;
}

export interface Edge {
  node: Node;
}

export interface KanvasoObjekto {
  edges: Edge[];
}

export interface UnuaNomo {
  enhavo: string;
}

export interface DuaNomo {
  enhavo: string;
}

export interface BildoF {
  url: string;
}

export interface Avataro {
  bildoF: BildoF;
}

export interface Familinomo {
  enhavo: string;
}

export interface PosedantoUzanto {
  unuaNomo: UnuaNomo;
  duaNomo: DuaNomo;
  uuid: string;
  sekso: string;
  avataro: Avataro;
  familinomo: Familinomo;
  objId: number;
}

export interface Node {
  tipo: Tipo;
  uuid: string;
  posedantoUzanto: PosedantoUzanto;
}

export interface Edge {
  node: Node;
}

export interface Posedanto {
  edges: Edge[];
}

export interface Node {
  pozicio?: any;
  uuid: string;
  forigo: boolean;
  publikigo: boolean;
  objId: number;
  nomo: Nomo;
  priskribo: Priskribo;
  tipo: Tipo;
  kategorio: Kategorio;
  kanvasoObjekto: KanvasoObjekto;
  posedanto: Posedanto;
}

export interface Edge {
  node: Node;
}

export interface KanvasojKanvaso {
  edges: Edge[];
}

export interface RootObject {
  kanvasojKanvaso: KanvasojKanvaso;
}

export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface Tipo {
  objId: number;
  nomo: Nomo;
  priskribo: Priskribo;
}

export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface PosedantoUzanto {
  uuid: string;
  objId: number;
}

export interface Node {
  uuid: string;
  forigo: boolean;
  publikigo: boolean;
  posedantoUzanto: PosedantoUzanto;
  tipo?: any;
}

export interface Edge {
  node: Node;
}

export interface Posedanto {
  edges: Edge[];
}

export interface Nomo {
  enhavo: string;
}

export interface Priskribo {
  enhavo: string;
}

export interface Kanvaso {
  uuid: string;
  forigo: boolean;
  publikigo: boolean;
  tipo?: any;
  pozicio?: any;
  posedanto: Posedanto;
  nomo: Nomo;
  priskribo: Priskribo;
  objId: number;
}

export interface Objekto {
  uuid: string;
  forigo: boolean;
  publikigo: boolean;
  kanvasojKanvasoobjektoligiloLigilo?: any;
  tipo: Tipo;
  pozicio?: any;
  nomo: Nomo;
  priskribo: Priskribo;
  objId: number;
  kanvaso: Kanvaso;
}

export interface KanvasoEventoj {
  evento: string;
  objekto: Objekto;
  objektoLigilo?: any;
}

export interface RootObject {
  kanvasoEventoj: KanvasoEventoj;
}
